package org.example.framework.exception;

public class CommonNameNotFoundException extends AuthenticationException {
  public CommonNameNotFoundException() {
  }

  public CommonNameNotFoundException(String message) {
    super(message);
  }

  public CommonNameNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public CommonNameNotFoundException(Throwable cause) {
    super(cause);
  }

  public CommonNameNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
