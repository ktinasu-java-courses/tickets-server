package org.example.framework.authentication;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LoginPasswordAuthentication implements Authentication {
  private final String login;
  private String password;
  private String role;

  @Override
  public String getLogin() {
    return login;
  }

  public String getRole() {
    return role;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public boolean isAnonymous() {
    return false;
  }

  @Override
  public void eraseCredentials() {
    password = "***";
  }
}
