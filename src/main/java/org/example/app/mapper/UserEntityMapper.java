package org.example.app.mapper;

import org.example.app.entity.UserEntity;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.mapstruct.Mapper;

@Mapper
public interface UserEntityMapper {
    LoginPasswordAuthentication toLoginPasswordAuthentication(
            final UserEntity entity);
}
