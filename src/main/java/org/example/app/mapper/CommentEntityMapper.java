package org.example.app.mapper;

import org.example.app.dto.CommentCreateRQ;
import org.example.app.dto.CommentCreateRS;
import org.example.app.dto.CommentGetByIdRS;
import org.example.app.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface CommentEntityMapper {
    CommentCreateRS toCommentCreateRS(final CommentEntity entity);

    @Mapping(target = "id", ignore = true)
    CommentEntity fromCommentCreateRQ(final CommentCreateRQ request, final String authorLogin);

    CommentGetByIdRS toCommentGetByIdRS(final CommentEntity entity);

    List<CommentGetByIdRS> toListOfCommentGetByIdRS(List<CommentEntity> entities);
}
