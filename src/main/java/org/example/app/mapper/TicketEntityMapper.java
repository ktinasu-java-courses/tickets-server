package org.example.app.mapper;

import org.example.app.dto.TicketCloseRS;
import org.example.app.dto.TicketCreateRQ;
import org.example.app.dto.TicketCreateRS;
import org.example.app.dto.TicketGetByIdRS;
import org.example.app.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface TicketEntityMapper {
    TicketGetByIdRS toTicketGetByIdRS(final TicketEntity entity);

    List<TicketGetByIdRS> toListOfTicketGetByIdRS(List<TicketEntity> entities);

    TicketCreateRS toTicketCreateRS(final TicketEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    TicketEntity fromTicketCreateRQ(final TicketCreateRQ request, final String authorLogin);

    TicketCloseRS toTicketCloseRS(final TicketEntity entity);
}
