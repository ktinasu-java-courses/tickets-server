package org.example.app.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.entity.UserEntity;
import org.example.app.mapper.UserEntityMapper;
import org.example.app.mapper.UserEntityMapperImpl;
import org.example.app.repository.UserRepository;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.authenticator.Authenticator;
import org.example.framework.exception.UserNotFoundException;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService implements Authenticator {
    private final UserRepository repository;
    private final Argon2PasswordEncoder passwordEncoder;
    private final UserEntityMapper mapper = new UserEntityMapperImpl();

    @Override
    public Authentication authenticate(String login, String password) {
        final UserEntity entity = repository.getByLogin(login).orElseThrow(UserNotFoundException::new);
        if (!passwordEncoder.matches(password, entity.getPassword())) {
            throw new UserNotFoundException();
        }
        entity.setPassword("***");
        final LoginPasswordAuthentication authentication = mapper.toLoginPasswordAuthentication(entity);
        authentication.eraseCredentials();
        return authentication;
    }
}
