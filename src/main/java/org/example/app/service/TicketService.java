package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.TicketEntity;
import org.example.app.exception.DataAccessException;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.mapper.TicketEntityMapper;
import org.example.app.mapper.TicketEntityMapperImpl;
import org.example.app.repository.TicketRepository;
import org.example.app.utils.UserRoles;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TicketService {
    private final TicketRepository repository;
    private final TicketEntityMapper mapper = new TicketEntityMapperImpl();

    public TicketGetByIdRS getById(final long id, final LoginPasswordAuthentication auth) {
        if (auth.getRole().equals(UserRoles.ROLE_SUPPORTER)) {
            final TicketEntity entity = repository.getById(id).orElseThrow(ItemNotFoundException::new);
            return mapper.toTicketGetByIdRS(entity);
        }
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        final TicketEntity entity = repository.getById(id).orElseThrow(ItemNotFoundException::new);
        if (!auth.getLogin().equals(entity.getAuthorLogin())) {
            throw new DataAccessException("not an author");
        }
        return mapper.toTicketGetByIdRS(entity);
    }

    public List<TicketGetByIdRS> getAll(final LoginPasswordAuthentication auth) {
        if (auth.getRole().equals(UserRoles.ROLE_SUPPORTER)) {
            final List<TicketEntity> entities = repository.getAll();
            return mapper.toListOfTicketGetByIdRS(entities);
        }
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        return getAllByAuthor(auth.getLogin());
    }

    public boolean isAuthor(final long id, final String authorLogin) {
        return getAllByAuthor(authorLogin).stream().anyMatch(o -> o.getId() == id);
    }

    public TicketCreateRS create(final TicketCreateRQ request, final LoginPasswordAuthentication auth) {
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        final TicketEntity entity = mapper.fromTicketCreateRQ(request, auth.getLogin());
        final TicketEntity ticket = repository.create(entity).orElseThrow(DataAccessException::new);
        return mapper.toTicketCreateRS(ticket);
    }

    public TicketCloseRS close(final TicketCloseRQ request, final LoginPasswordAuthentication auth) {
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        if (!isAuthor(request.getId(), auth.getLogin())) {
            throw new DataAccessException("not an owner");
        }
        final TicketEntity entity = repository.close(request.getId()).orElseThrow(DataAccessException::new);
        return mapper.toTicketCloseRS(entity);
    }

    private List<TicketGetByIdRS> getAllByAuthor(final String authorLogin) {
        final List<TicketEntity> entities = repository.getAllByAuthor(authorLogin);
        return mapper.toListOfTicketGetByIdRS(entities);
    }
}
