package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.CommentEntity;
import org.example.app.exception.DataAccessException;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.mapper.CommentEntityMapper;
import org.example.app.mapper.CommentEntityMapperImpl;
import org.example.app.repository.CommentRepository;
import org.example.app.utils.UserRoles;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CommentService {
    private final CommentRepository repository;
    private final TicketService ticketService;
    private final CommentEntityMapper mapper = new CommentEntityMapperImpl();

    public CommentCreateRS create(final CommentCreateRQ request, final LoginPasswordAuthentication auth) {
        if (auth.getRole().equals(UserRoles.ROLE_SUPPORTER)) {
            final CommentEntity entity = mapper.fromCommentCreateRQ(request, auth.getLogin());
            final CommentEntity comment = repository.create(entity).orElseThrow(DataAccessException::new);
            return mapper.toCommentCreateRS(comment);
        }
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        if (!ticketService.isAuthor(request.getTicketId(), auth.getLogin())) {
            throw new DataAccessException("not an owner");
        }
        final CommentEntity entity = mapper.fromCommentCreateRQ(request, auth.getLogin());
        final CommentEntity comment = repository.create(entity).orElseThrow(DataAccessException::new);
        return mapper.toCommentCreateRS(comment);
    }

    public CommentGetByIdRS getById(final long id, final LoginPasswordAuthentication auth) {
        if (auth.getRole().equals(UserRoles.ROLE_SUPPORTER)) {
            final CommentEntity entity = repository.getById(id).orElseThrow(ItemNotFoundException::new);
            return mapper.toCommentGetByIdRS(entity);
        }
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        final CommentEntity entity = repository.getById(id).orElseThrow(ItemNotFoundException::new);
        if (!ticketService.isAuthor(entity.getTicketId(), auth.getLogin())) {
            throw new DataAccessException("not an owner");
        }
        return mapper.toCommentGetByIdRS(entity);
    }

    public List<CommentGetByIdRS> getAllByTicket(final long ticketId, final LoginPasswordAuthentication auth) {
        if (auth.getRole().equals(UserRoles.ROLE_SUPPORTER)) {
            final List<CommentEntity> comments = repository.getAllByTicket(ticketId);
            return mapper.toListOfCommentGetByIdRS(comments);
        }
        if (!auth.getRole().equals(UserRoles.ROLE_USER)) {
            throw new DataAccessException("not a user");
        }
        if (!ticketService.isAuthor(ticketId, auth.getLogin())) {
            throw new DataAccessException("not an owner");
        }
        final List<CommentEntity> comments = repository.getAllByTicket(ticketId);
        return mapper.toListOfCommentGetByIdRS(comments);
    }
}
