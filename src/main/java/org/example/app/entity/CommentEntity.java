package org.example.app.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class CommentEntity {
    private long id;
    private String authorLogin;
    private String text;
    private long ticketId;
}
