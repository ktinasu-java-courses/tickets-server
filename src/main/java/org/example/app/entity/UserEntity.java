package org.example.app.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class UserEntity {
  private long id;
  private String login;
  private String password;
  private String role;
}
