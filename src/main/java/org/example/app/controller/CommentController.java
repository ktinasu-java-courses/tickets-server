package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.dto.CommentCreateRQ;
import org.example.app.dto.CommentCreateRS;
import org.example.app.dto.CommentGetByIdRS;
import org.example.app.service.CommentService;
import org.example.app.utils.LoginPasswordAuthentications;
import org.example.app.utils.QueryStrings;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.example.framework.routes.RoutesMap.routes;

@Controller
public class CommentController {
    private final CommentService service;
    private final Gson gson;

    public CommentController(CommentService service, Gson gson) {
        this.service = service;
        this.gson = gson;
        routes.put("/comments.create", this::create);
        routes.put("/comments.getById", this::getById);
        routes.put("/comments.getAllByTicket", this::getAllByTicket);
    }

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final CommentCreateRQ requestBody = gson.fromJson(req.getReader(), CommentCreateRQ.class);
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final CommentCreateRS comment = service.create(requestBody, auth);
        writer.println(gson.toJson(comment));
        writer.flush();
    }

    public void getById(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final long id = QueryStrings.getId(req.getQueryString());
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final CommentGetByIdRS comment = service.getById(id, auth);
        writer.println(gson.toJson(comment));
        writer.flush();
    }

    public void getAllByTicket(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final long ticketId = QueryStrings.getId(req.getQueryString());
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final List<CommentGetByIdRS> comments = service.getAllByTicket(ticketId, auth);
        writer.println(gson.toJson(comments));
        writer.flush();
    }
}
