package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.dto.*;
import org.example.app.service.TicketService;
import org.example.app.utils.LoginPasswordAuthentications;
import org.example.app.utils.QueryStrings;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.example.framework.routes.RoutesMap.routes;

@Controller
public class TicketController {
    private final TicketService service;
    private final Gson gson;

    public TicketController(TicketService service, Gson gson) {
        this.service = service;
        this.gson = gson;
        routes.put("/tickets.getById", this::getById);
        routes.put("/tickets.getAll", this::getAll);
        routes.put("/tickets.create", this::create);
        routes.put("/tickets.close", this::close);
    }

    public void getById(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final long id = QueryStrings.getId(req.getQueryString());
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final TicketGetByIdRS ticket = service.getById(id, auth);
        writer.print(gson.toJson(ticket));
        writer.flush();
    }

    public void getAll(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final List<TicketGetByIdRS> tickets = service.getAll(auth);
        writer.print(gson.toJson(tickets));
        writer.flush();
    }

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final TicketCreateRQ requestBody = gson.fromJson(req.getReader(), TicketCreateRQ.class);
        final TicketCreateRS ticket = service.create(requestBody, auth);
        writer.print(gson.toJson(ticket));
        writer.flush();
    }

    public void close(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final LoginPasswordAuthentication auth = LoginPasswordAuthentications.castAuthToLoginPasswordAuth(req);
        final PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final TicketCloseRQ requestBody = gson.fromJson(req.getReader(), TicketCloseRQ.class);
        final TicketCloseRS ticket = service.close(requestBody, auth);
        writer.print(gson.toJson(ticket));
        writer.flush();
    }
}
