package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentGetByIdRS {
  private long id;
  private String authorLogin;
  private String text;
  private long ticketId;
}
