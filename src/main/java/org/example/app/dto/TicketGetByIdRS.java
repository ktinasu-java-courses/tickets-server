package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketGetByIdRS {
  private long id;
  private String name;
  private String content;
  private String status;
  private String authorLogin;
}
