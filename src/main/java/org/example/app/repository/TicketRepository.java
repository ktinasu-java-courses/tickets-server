package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.TicketEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class TicketRepository {
    private final Jdbi jdbi;

    public Optional<TicketEntity> getById(final long id) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "select id, name, content, status, author_login from tickets " +
                                "where id=:id order by status desc, id"
                )
                .bind("id", id)
                .mapToBean(TicketEntity.class)
                .findFirst());
    }

    public List<TicketEntity> getAllByAuthor(final String authorLogin) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "select id, name, content, status, author_login from tickets " +
                                "where author_login=:author_login " +
                                "order by status desc, id"
                )
                .bind("author_login", authorLogin)
                .mapToBean(TicketEntity.class)
                .stream().collect(Collectors.toList()));
    }

    public List<TicketEntity> getAll() {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "select id, name, content, status, author_login from tickets " +
                                "order by status desc, id"
                )
                .mapToBean(TicketEntity.class)
                .stream().collect(Collectors.toList()));
    }

    public Optional<TicketEntity> create(final TicketEntity ticket) {
        Optional<TicketEntity> entity = null;
        try {
            entity = jdbi.
                    withHandle(handle -> handle.createQuery(
                                    //languge=PostgreSQL
                                    "insert into tickets (name, content, author_login) " +
                                            "values (:name, :content, :author_login) returning *"
                            )
                            .bind("name", ticket.getName())
                            .bind("content", ticket.getContent())
                            .bind("author_login", ticket.getAuthorLogin())
                            .mapToBean(TicketEntity.class)
                            .findFirst());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public Optional<TicketEntity> close(final long id) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "update tickets set status = 'closed' where id=:id returning *"
                )
                .bind("id", id)
                .mapToBean(TicketEntity.class)
                .findFirst());
    }
}
