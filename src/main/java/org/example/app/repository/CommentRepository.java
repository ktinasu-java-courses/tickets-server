package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class CommentRepository {
    private final Jdbi jdbi;

    public Optional<CommentEntity> getById(final long id) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "select id, author_login, text, ticket_id from comments where id=:id"
                )
                .bind("id", id)
                .mapToBean(CommentEntity.class)
                .findFirst());
    }

    public List<CommentEntity> getAllByTicket(final long ticketId) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "select id, author_login, text, ticket_id from comments where ticket_id=:ticket_id"
                )
                .bind("ticket_id", ticketId)
                .mapToBean(CommentEntity.class)
                .stream().collect(Collectors.toList()));
    }

    public Optional<CommentEntity> create(final CommentEntity comment) {
        return jdbi.
                withHandle(handle -> handle.createQuery(
                                //languge=PostgreSQL
                                "insert into comments (author_login, text, ticket_id) " +
                                        "values (:author_login, :text, :ticket_id) returning *"
                        )
                        .bind("author_login", comment.getAuthorLogin())
                        .bind("text", comment.getText())
                        .bind("ticket_id", comment.getTicketId())
                        .mapToBean(CommentEntity.class)
                        .findFirst());
    }
}
