package org.example.app.exception;

public class UnsupportedQueryStringException extends RuntimeException {
    public UnsupportedQueryStringException() {
    }

    public UnsupportedQueryStringException(String message) {
        super(message);
    }

    public UnsupportedQueryStringException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedQueryStringException(Throwable cause) {
        super(cause);
    }

    public UnsupportedQueryStringException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
