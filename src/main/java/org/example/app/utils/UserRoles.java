package org.example.app.utils;

public class UserRoles {
    public static final String ROLE_USER = "user";
    public static final String ROLE_SUPPORTER = "supporter";
}
