package org.example.app.utils;

import org.example.app.exception.UnsupportedQueryStringException;

public class QueryStrings {
    public static long getId(final String queryString) {
        final String[] split = queryString.split("=");
        if (split.length < 2) {
            throw new UnsupportedQueryStringException();
        }
        if (!split[0].equals("id")) {
            throw new UnsupportedQueryStringException();
        }
        return Long.parseLong(split[1]);
    }
}
