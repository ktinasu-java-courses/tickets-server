package org.example.app.utils;

import jakarta.servlet.http.HttpServletRequest;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.exception.AuthenticationException;

public class LoginPasswordAuthentications {
    public static LoginPasswordAuthentication castAuthToLoginPasswordAuth(HttpServletRequest req) {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        if (!(authentication instanceof LoginPasswordAuthentication)) {
            throw new AuthenticationException("not a loginPassword authentication");
        }
        return (LoginPasswordAuthentication) authentication;
    }
}
