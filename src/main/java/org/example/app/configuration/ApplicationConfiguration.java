package org.example.app.configuration;

import com.google.gson.Gson;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
public class ApplicationConfiguration {
    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public Argon2PasswordEncoder passwordEncoder() {
        return new Argon2PasswordEncoder();
    }

    @Bean
    public Jdbi jdbi() throws NamingException {
        final JndiTemplate template = new JndiTemplate();
        final DataSource dataSource = template.lookup("java:/comp/env/jdbc/db", DataSource.class);
        return Jdbi.create(dataSource);
    }

}
