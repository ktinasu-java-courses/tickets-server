insert into users (login, password, role)
values ('vasya', '$argon2id$v=19$m=4096,t=3,p=1$OjQRD/pI2A8nYrwk+eDZ5g$YqK1fmVDT0VxBtmnd79SPMEW2WMwkS07nsBI4dq+t/k', 'user'),
       ('petya', '$argon2id$v=19$m=4096,t=3,p=1$OjQRD/pI2A8nYrwk+eDZ5g$YqK1fmVDT0VxBtmnd79SPMEW2WMwkS07nsBI4dq+t/k', 'user'),
       ('masha', '$argon2id$v=19$m=4096,t=3,p=1$OjQRD/pI2A8nYrwk+eDZ5g$YqK1fmVDT0VxBtmnd79SPMEW2WMwkS07nsBI4dq+t/k', 'supporter'),
       ('dasha', '$argon2id$v=19$m=4096,t=3,p=1$OjQRD/pI2A8nYrwk+eDZ5g$YqK1fmVDT0VxBtmnd79SPMEW2WMwkS07nsBI4dq+t/k', 'supporter'),
       ('vanya', '$argon2id$v=19$m=4096,t=3,p=1$OjQRD/pI2A8nYrwk+eDZ5g$YqK1fmVDT0VxBtmnd79SPMEW2WMwkS07nsBI4dq+t/k', 'user');

insert into tickets (name, content, status, author_login)
values ('name', 'content', 'open', 'vasya');

