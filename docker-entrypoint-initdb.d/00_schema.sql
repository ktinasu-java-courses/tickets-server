create table users
(
    login    text primary key,
    password text not null,
    role     text not null default 'user'
);

create table tickets
(
    id           bigserial primary key,
    name         text not null,
    content      text not null,
    status       text not null default 'open',
    author_login text not null references users (login)
);

create table comments
(
    id           bigserial primary key,
    author_login text   not null references users (login),
    text         text   not null,
    ticket_id    bigint not null references tickets
);